﻿/*
 * Envelope filtering based on the work of Nigel Redmon.
 * EarLevel Engineering: earlevel.com
 * Copyright 2012 Nigel Redmon
 * 
 * The following code is derivative of the code presented at:
 * http://www.earlevel.com/main/2013/06/01/envelope-generators/
 * 
 */
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum EnvelopeState {Idle, Attack, Decay, Sustain, Release};

[Serializable]
public class Envelope{
	[Range(0.001f,5)]
	public float attackRate = 2;
	private float prevAttackRate;
	private float attackSamples = 2;
	private float attackCoef = 0;
	private float attackBase = 0;
	private float targetRatioA = 0;

	[Range(0.001f,5)]
	public float decayRate = 0.1f;
	private float prevDecayRate;
	private float decaySamples = 0.1f;
	private float decayCoef = 0.1f;
	private float decayBase = 0;
	private float targetRatioDR = 0;

	[Range(0,1)]
	[HideInInspector]
	public float attackLevel = 1;
	[Range(0,1)]
	public float sustainLevel = 0.5f;

	[Range(0.001f,5)]
	public float releaseRate = 0.1f;
	private float prevReleaseRate;
	private float releaseSamples = 0.1f;
	private float releaseCoef = 0;
	private float releaseBase = 0;
	
	public Envelope(float a, float d, float s, float r){
		Initialize();
	}

	float output = 0;
	public float Gate{
		get{return output;}
	}

	//*
	public float Process(float signal) {
		if(attackRate!=prevAttackRate){
			SetAttackRate(attackRate);
		}
		if(decayRate!=prevDecayRate){
			SetDecayRate(decayRate);
		}
		if(releaseRate!=prevReleaseRate){
			SetReleaseRate(releaseRate);
		}
		switch (envelopeState) {
		case EnvelopeState.Idle:
			break;
		case EnvelopeState.Attack:
			output = attackBase + output * attackCoef;
			if (output >= 1.0f) {
				output = 1.0f;
				envelopeState = EnvelopeState.Decay;
			}
			break;
		case EnvelopeState.Decay:
			output = decayBase + output * decayCoef;
			if (output <= sustainLevel) {
				output = sustainLevel;
				envelopeState = EnvelopeState.Sustain;
			}
			break;
		case EnvelopeState.Sustain:
			break;
		case EnvelopeState.Release:
			output = releaseBase + output * releaseCoef;
			if (output <= 0.0f) {
				output = 0.0f;
				envelopeState = EnvelopeState.Idle;
			}
			break;
		}
		return output * signal;
	}
	//*/
	
	public void Initialize(){
		SetTargetRatioA(0.3f);
		SetTargetRatioDR(0.0001f);

		/*
		SetAttackRate(0.05);
		SetDecayRate(0.3);
		SetReleaseRate(0);
		SetSustainLevel(0.2);
		*/
	}
	
	public void SetAttackRate(float rate){
		attackRate = rate;
		prevAttackRate = attackRate;
		attackSamples = attackRate * AudioController.Instance.sampleRate;
		attackCoef = CalcCoef(attackSamples, targetRatioA);
		attackBase = (1 + targetRatioA) * (1 - attackCoef);
	}
	
	public void SetDecayRate(float rate){
		decayRate = rate;
		prevDecayRate = decayRate;
		decaySamples = decayRate * AudioController.Instance.sampleRate;
		decayCoef = CalcCoef(decaySamples, targetRatioDR);
		decayBase = (sustainLevel - targetRatioDR) * (1 - decayCoef);
	}
	
	public void SetReleaseRate(float rate){
		releaseRate = rate;
		prevReleaseRate = releaseRate;
		releaseSamples = releaseRate * AudioController.Instance.sampleRate;
		releaseCoef = CalcCoef(releaseSamples, targetRatioDR);
		releaseBase = -targetRatioDR * (1 - releaseCoef);
	}
	
	public void SetSustainLevel(float level){
		sustainLevel = level;
		decayBase = (sustainLevel - targetRatioDR) * (1 - decayCoef);
	}
	
	public void SetTargetRatioA(float targetRatio){
		if(targetRatio < 0.000000001f)
			targetRatio = 0.000000001f;
		targetRatioA = targetRatio;
		attackBase = (1 + targetRatioA) * (1 - attackCoef);
	}
	
	public void SetTargetRatioDR(float targetRatio){
		if(targetRatio < 0.000000001f)
			targetRatio = 0.000000001f;
		targetRatioDR = targetRatio;
		decayBase = (sustainLevel - targetRatioDR) * (1 - decayCoef);
		releaseBase = -targetRatioDR * (1 - releaseCoef);
	}
	
	private float CalcCoef(float rate, float targetRatio){
		return Mathf.Exp(-Mathf.Log((1+targetRatio) / targetRatio) / rate);
	}

	public EnvelopeState envelopeState = EnvelopeState.Idle;
	public void SetState(EnvelopeState state){
		this.envelopeState = state;
	}
}