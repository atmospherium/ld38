﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ModeController : MonoBehaviour {
	Dropdown dropdown;

	// Use this for initialization
	void Start () {
		dropdown = GetComponent<Dropdown>();
		foreach (Mode mode in Enum.GetValues(typeof(Mode))){
			dropdown.options.Add(new Dropdown.OptionData(){text=mode.ToString()});
		}
		AudioController.Instance.mode = Mode.PhrygianDominant;
		dropdown.value = (int)AudioController.Instance.mode;
		dropdown.onValueChanged.AddListener((int val)=>{
			AudioController.Instance.mode = (Mode)val;
		});
	}
	
}
