﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PatternController : MonoBehaviour {
	Dropdown dropdown;

	// Use this for initialization
	void Start () {
		dropdown = GetComponent<Dropdown>();
		for(int i = 0; i < AudioController.Instance.patternCount; i++){
			dropdown.options.Add(new Dropdown.OptionData(){text="Pattern "+(i+1).ToString()});
		}
		AudioController.Instance.currentPattern = 0;
		dropdown.value = 0;
		dropdown.onValueChanged.AddListener((int val)=>{
			AudioController.Instance.currentPattern = val;
		});
	}
	
}
