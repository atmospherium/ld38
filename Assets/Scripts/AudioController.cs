﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public enum Mode{
	Chromatic,
	Lydian, Ionian, Mixolydian, Dorian, Aeolian, Phrygian, Locrian,
	WholeTone, Diminished1, Diminished2,
	PhrygianDominant, Enigmatic, Persian
}

public class AudioController : MonoBehaviour {
	public Mode mode;
	Dictionary<Mode, int[]> modeDef = new Dictionary<Mode, int[]>();
	public HexController_ScriptableObject[] hexControllers;
	private static AudioController _instance;
	public static AudioController Instance{
		get{
			return _instance;
		}
	}
	float frequency = 440f;
	public int sampleRate;

	// Use this for initialization
	bool loaded = false;

	public AudioClip kick;
	float[] kickSample;
	int kickIndex;
	float kickVol;

	public AudioClip snare;
	float[] snareSample;
	int snareIndex;
	float snareVol;

	public AudioClip hat;
	float[] hatSample;
	int hatIndex;
	float hatVol;
	void Start () {
		_instance = this;
		sampleRate = AudioSettings.outputSampleRate;

		kickSample = new float[kick.samples*2];
		kick.GetData(kickSample, 0);
		kickIndex = kickSample.Length-1;

		snareSample = new float[snare.samples*2];
		snare.GetData(snareSample, 0);
		snareIndex = snareSample.Length - 1;

		hatSample = new float[hat.samples*2];
		hat.GetData(hatSample, 0);
		hatIndex = hatSample.Length - 1;
		

		modeDef.Add(Mode.Chromatic, new int[12]{0,1,2,3,4,5,6,7,8,9,10,11});
		modeDef.Add(Mode.Lydian, new int[7]{0,2,4,6,7,9,11});
		modeDef.Add(Mode.Ionian, new int[7]{0,2,4,5,7,9,11});
		modeDef.Add(Mode.Mixolydian, new int[7]{0,2,4,5,7,9,10});
		modeDef.Add(Mode.Dorian, new int[7]{0,2,3,5,7,9,10});
		modeDef.Add(Mode.Aeolian, new int[7]{0,2,3,5,7,8,10});
		modeDef.Add(Mode.Phrygian, new int[7]{0,1,3,5,7,8,10});
		modeDef.Add(Mode.Locrian, new int[7]{0,1,3,5,6,8,10});
		modeDef.Add(Mode.WholeTone, new int[6]{0,2,4,6,8,10});
		modeDef.Add(Mode.Diminished1, new int[8]{0,1,3,4,6,7,9,10});
		modeDef.Add(Mode.Diminished2, new int[8]{0,2,3,5,6,8,9,11});
		modeDef.Add(Mode.PhrygianDominant, new int[7]{0,1,4,5,7,8,10});
		modeDef.Add(Mode.Enigmatic, new int[7]{0,1,4,6,8,10,11});
		modeDef.Add(Mode.Persian, new int[7]{0,1,4,5,6,9,10});

		loaded = true;
	}

	void Update () {
		foreach(HexController_ScriptableObject hexController in hexControllers){
			hexController.IsActive = Array.IndexOf(modeDef[mode],hexController.pitchOffset) >= 0;
		}
		Camera.main.orthographicSize *= 1f + (0.01f * sEnvelope.Gate);
	}

	float sample;
	float index;
	
	[HideInInspector]
	public readonly int patternCount = 4;
	[HideInInspector]
	public readonly int patternLength = 16;
	[HideInInspector]
	public int currentPattern = 3;
	public float?[,] kickPattern = new float?[4,16]{
		{null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null},
		{1, null, null, 0.5f, null, null, null, null, 1, null, null, null, null, null, 1, null },
		{1, null, null, 0.5f, null, null, null, 0.8f, 1, null, null, null, null, null, 1, null },
		{1, null, null, 0.5f, null, null, null, 0.8f, 1, null, null, null, null, null, 1, null }
	};
	public float?[,] snarePattern = new float?[4,16]{
		{null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null},
		{null, null, null, null, 1, null, null, 0.3f, null, null, null, null, 1, null, 0.4f, null },
		{null, null, null, null, 1, null, null, null, null, null, null, null, 1, null, null, 0.4f },
		{null, null, null, null, 1, null, null, null, null, null, null, null, 1, null, null, 0.4f }
	};
	public float?[,] hatPattern = new float?[4,16]{
		{null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null},
		{1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f},
		{1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f},
		{1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f, 1, 0.25f, 0.5f, 0.25f}
	};

	public int?[,] subPattern = new int?[4,16]{
		{0, null, null, null, null, null, null, null, 0, null, null, null, null, null, 4, null },
		{0, null, null, null, null, null, null, null, 0, null, null, null, null, null, 4, null },
		{0, null, null, 0, null, null,null,null,null, null, null, 0, 1, null, 2, null},
		{9,null,null,6,null,null,2,null,null,2,null,null,2,null,2,null}
	};

	public int?[,] bassPattern = new int?[4,16]{
		{0, null, null, 0, null, null, 2, 1, 0, null, null, 0, null, null, 0, null},
		{0, null, null, 0, null, null, 2, 1, 0, null, null, 0, null, null, 0, null},
		{0, null, null, 0, null, null,null,null,null, null, null, 0, 1, null, 2, null},
		{9,null,null,6,null,null,2,null,null,2,null,null,2,null,2,null}
	};

	public int?[,] melodyPattern = new int?[4,16]{
		{null, 4, 4, null, 5, 5, null, null, null, 6, 6, null, 3, 3, 2, 1},
		{null, 4, 4, null, 5, 5, null, null, null, 6, 6, null, 3, 3, 2, 1},
		{0, null, null, 0, null, null,null,null,null, null, null, 0, 1, null, 2, null},
		{9,3,3,6,5,2,2,1,3,2,2,1,2,null,2,null}
	};

	public float amplitude = 0;

	private int _offset = 0;
	private int _bOffset = 0;
	private int _sOffset = 0;
	public void SetOffset(int offset){
		_offset = offset;
	}


	float subSample;
	float subIndex;
	float droneSample;
	float droneIndex = 0;

	public float sixteenth = 480f;
	int beatIndex = 0;

	float dAmplitude = 1;
	float sAmplitude = 1;

	[SerializeField]
	public Envelope sEnvelope = new Envelope(0.1f, 0.2f, 0.3f, 0.0f);
	[SerializeField]
	public Envelope bEnvelope = new Envelope(0.1f, 0.2f, 0.3f, 0);
	[SerializeField]
	public Envelope mEnvelope = new Envelope(0.1f, 0.2f, 0.3f, 0);
	bool first = true;
	void OnAudioFilterRead(float[] data, int channels){
		for(int i = 0; i < data.Length; i+=channels){
				//dAmplitude = 1f;
			if(beatIndex % (int)Mathf.Round(60*sampleRate/sixteenth) == 0){
				var bIn = (beatIndex / (int)Mathf.Round(60 * sampleRate/sixteenth)) % patternLength;
				if (bIn == 0) {
					if(first){
						first = false;
					} else{
						first = true;
						currentPattern = (int)Mathf.Repeat(currentPattern + 1, patternCount-1);
					}
				}
				if(subPattern[currentPattern, bIn] != null){
					sEnvelope.SetState(EnvelopeState.Attack);
					_sOffset = modeDef[mode][(int)subPattern[currentPattern, bIn]%modeDef[mode].Length]; 
				}
				if(bassPattern[currentPattern, bIn] != null){
					bEnvelope.SetState(EnvelopeState.Attack);
					_bOffset = modeDef[mode][(int)bassPattern[currentPattern, bIn]%modeDef[mode].Length]; 
				}
				if(melodyPattern[currentPattern, bIn] != null){
					mEnvelope.SetState(EnvelopeState.Attack);
					_offset = modeDef[mode][(int)melodyPattern[currentPattern, bIn]%modeDef[mode].Length];
					hexControllers[_offset].SetState(HexState.Click);
				}
				if(kickPattern[currentPattern, bIn] != null){
					kickVol = (float)kickPattern[currentPattern, bIn];
					kickIndex = 0;
				}
				if(snarePattern[currentPattern, bIn] != null){
					snareVol = (float)snarePattern[currentPattern, bIn];
					snareIndex = 0;
				}
				if(hatPattern[currentPattern, bIn] != null){
					hatVol = (float)hatPattern[currentPattern, bIn];
					hatIndex = 0;
				}
			}

			kickIndex = (int)Mathf.Clamp(kickIndex + 2, 0, kickSample.Length-1);
			snareIndex = (int)Mathf.Clamp(snareIndex + 2, 0, snareSample.Length-1);
			hatIndex = (int)Mathf.Clamp(hatIndex + 2, 0, hatSample.Length-1);

			droneIndex = Mathf.Repeat(droneIndex + frequency *Mathf.Pow(2, (float)_bOffset / 12f) / 4 / sampleRate, 1f);
			droneSample = bEnvelope.Process(
				Mathf.Sin(4 * Mathf.PI * droneIndex)// + Mathf.Sin(2 * Mathf.PI * droneIndex);
			);

			subIndex = Mathf.Repeat(subIndex + frequency *Mathf.Pow(2, (float)_sOffset / 12f) / 8 / sampleRate, 1f);
			subSample = sEnvelope.Process(
				Mathf.Sin(2 * Mathf.PI * subIndex)
			);

			index = Mathf.Repeat(index + frequency * Mathf.Pow(2, (float)_offset / 12f) / sampleRate, 1);
			sample = mEnvelope.Process(
				Mathf.Sin((2 )* Mathf.PI * index)
			);
			for(int c = 0; c < channels; c++){
				data[i+c] = (
					subSample * 0.8f + 
					droneSample * 0.7f + 
					sample * 0.7f + 
					kickSample[kickIndex] * kickVol * 0.7f + 
					snareSample[snareIndex] * snareVol * 0.6f + 
					hatSample[hatIndex] * hatVol * 0.7f
				) * 0.7f;
			}
			
			beatIndex += 1;
		}
	}
}
