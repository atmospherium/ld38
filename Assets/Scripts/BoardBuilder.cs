﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardBuilder : MonoBehaviour {
	Vector3 offset_vertical = new Vector3(0, -12 * 46);
	Vector3 offset_major = new Vector3(39.84f, 23 + (8 * 46));
	Vector3 offset_minor = new Vector3(39.84f, -23 - (3 * 46));

	Vector3 farthestRight = new Vector3(0, 0);
	Vector3 farthestLeft = new Vector3(0, 0);

	public GameObject octave;

	public class VerticalRow {
		GameObject row;
		float max; //F#
		float min; //C#
		GameObject octave;

		int index_neg = 1;
		int index_pos = 0;

		public float Max{
			get{return max;}
		}

		public float Min{
			get{return min;}
		}

		public string Name{
			get{return row.name;}
		}

		public VerticalRow(GameObject row, GameObject octave){
			this.row = row;
			this.octave = octave;
			this.CreateOctave(Vector3.zero, false);
			this.max = row.transform.GetChild(0).FindChild("F#").gameObject.transform.position.y;
			this.min = row.transform.GetChild(0).FindChild("C#").gameObject.transform.position.y;
		}

		public void CreateOctave(Vector3 offset, bool CheckMinMax = true){
			var newOffset = offset;
			if(CheckMinMax){
				newOffset = offset.y < 0 ? offset * index_pos++ : offset * index_neg++;
			}
			var child = Instantiate(octave, row.transform.position + newOffset, row.transform.rotation);
			child.transform.parent = row.transform;
			if(CheckMinMax){
				this.max = Mathf.Max(this.max, child.transform.FindChild("F#").position.y);
				this.min = Mathf.Min(this.min, child.transform.FindChild("C#").position.y);
			}
		}

		// public AddOctave(GameObject octave){
		// 	this.max = Mathf.Max()
		// }
	}

	List<VerticalRow> rows = new List<VerticalRow>();
	// Use this for initialization
	void Start () {
		this.BuildBoard();
	}

	void Update(){
		float camSize = Camera.main.orthographicSize;
		Vector2 farBorder = new Vector2(camSize * Screen.width / Screen.height, camSize * Screen.height / Screen.width);
		if(-farBorder.x * 1.1f < farthestLeft.x){
			farthestLeft -= farthestLeft.y > 0 ? offset_major : offset_minor;
			CreateRow(farthestLeft);
		}
		if(farBorder.x * 1.1f > farthestRight.x){
			farthestRight += farthestRight.y < 0 ? offset_major : offset_minor;
			CreateRow(farthestRight);
		}
		rows.ForEach((VerticalRow row)=>{
			if(row.Max < farBorder.y * 2f){
				row.CreateOctave(-offset_vertical);
			}else if(row.Min > -farBorder.y * 2f){
				row.CreateOctave(offset_vertical);
			}
		});
	}

	void BuildBoard(){
		float camSize = Camera.main.orthographicSize;
		Vector2 farBorder = new Vector2(camSize * Screen.width / Screen.height, camSize * Screen.height / Screen.width);
		Vector3 pos = new Vector3( -farBorder.x, 0);
		while(pos.x < farBorder.x){
			if(pos.x < farthestLeft.x){
				farthestLeft = pos;
			}
			if(pos.x > farthestRight.x){
				farthestRight = pos;
			}
			CreateRow(pos);
			if(pos.y < 0){
				pos += offset_major;
			}else{
				pos += offset_minor;
			}
		}
	}

	void CreateRow(Vector3 pos){
		var row = new GameObject();
		row.transform.position = pos;
		row.transform.parent = gameObject.transform;
		row.name = "Row: "+pos.x;
		rows.Add(new VerticalRow(row, octave));
	}
}
