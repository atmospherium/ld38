﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneSignal : MonoBehaviour {
	float droneSample;
	float droneIndex = 0;

	public float frequency = 440f;
	int sampleRate;

	// Use this for initialization
	void Start () {
		sampleRate = AudioSettings.outputSampleRate;
	}

	void Update(){
		Camera.main.backgroundColor = Color.Lerp(Color.black, new Color(0.6f, 0.4f, 0), amplitude);
	}

	public float quarter = 120f;
	float beatIndex = 0;

	float amplitude = 1;
}
