﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTrigger : MonoBehaviour {
	public int pitchOffset;
	private Renderer rend;
	public HexController_ScriptableObject hexController;
	private Rigidbody2D rb;
	private CircleCollider2D collider;
    void Start() {
        rend = GetComponent<Renderer>();
		rb = GetComponent<Rigidbody2D>();
		collider = GetComponent<CircleCollider2D>();
		hexController.SetState(HexState.Normal);
    }

	void Update(){
		if(hexController.resetToNormal){
			hexController.timeToNormal -= Time.deltaTime;
			if(hexController.timeToNormal < 0){
				hexController.resetToNormal = false;
				hexController.SetState(HexState.Normal);
			}
		}

		if(hexController.IsActive){
			transform.localScale = new Vector3(0.9f,0.9f,0.9f);
			collider.isTrigger = true;
		}else{
			transform.localScale = new Vector3(1f, 1f, 1f);
			collider.isTrigger = false;
		}
	}

	void LateUpdate(){
		rend.material.SetColor("_Color", hexController.Current);
	}
    void OnMouseEnter() {
		Debug.Log("ON MOUSE ENTER");
		hexController.SetState(HexState.Hover);
		//AudioController.Instance.SetOffset(this.pitchOffset);
    }

	void OnMouseDown(){
		hexController.SetState(HexState.Click);
	}

	void OnMouseUp(){
		hexController.SetState(HexState.Hover);
	}

    void OnMouseExit() {
        hexController.SetState(HexState.Normal);
    }

	void OnTriggerEnter2D(Collider2D other){
		hexController.SetState(HexState.Hover);
	}

	void OnTriggerExit2D(Collider2D other){
		hexController.SetState(HexState.Normal);
	}
}
