﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomOutScript : MonoBehaviour {
	private float _targetSize = 0;

	public float startSize = 100;
	public float maxZoom = 4000;
	[Range(0f, 0.2f)]
	public float zoomSpeed = 0.02f;
	Camera cam;
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
		_targetSize = startSize;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		_targetSize = Mathf.Clamp(_targetSize * (1 + zoomSpeed * Time.deltaTime), 0, maxZoom);
		cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, _targetSize, 10f * Time.deltaTime);
	}
}
