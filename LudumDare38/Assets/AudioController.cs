﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
	private static AudioController _instance;
	public static AudioController Instance{
		get{
			return _instance;
		}
	}
	float frequency = 440f;
	int sampleRate;

	// Use this for initialization
	void Start () {
		_instance = this;
		sampleRate = AudioSettings.outputSampleRate;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	float sample;
	float index;
	public float amplitude = 0;

	private int _offset = 0;
	public void SetOffset(int offset){
		_offset = offset;
	}
	void OnAudioFilterRead(float[] data, int channels){
		for(int i = 0; i < data.Length; i+=channels){
			index = Mathf.Repeat(index + frequency * Mathf.Pow(2, (float)_offset / 12f) / sampleRate, 1);
			sample = Mathf.Sin(2* Mathf.PI * index);
			amplitude = Mathf.Clamp(amplitude * 0.9999f, 0.03f, 1);
			for(int c = 0; c < channels; c++){
				data[i+c] = sample * amplitude;
			}

		}
	}
}
