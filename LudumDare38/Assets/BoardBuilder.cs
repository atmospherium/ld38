﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardBuilder : MonoBehaviour {
	Vector3 offset_vertical = new Vector3(0, -12 * 46);
	Vector3 offset_major = new Vector3(39.84f, 23 + (8 * 46));
	Vector3 offset_minor = new Vector3(39.84f, -23 - (3 * 46));

	Vector3 farthestRight = new Vector3(0, 0);
	Vector3 farthestLeft = new Vector3(0, 0);

	public GameObject octave;
	// Use this for initialization
	void Start () {
		this.BuildBoard();
	}

	void Update(){
		float camSize = Camera.main.orthographicSize;
		float farBorder = camSize * Screen.width / Screen.height;
		if(-farBorder * 1.1f < farthestLeft.x){
			farthestLeft -= farthestLeft.y > 0 ? offset_major : offset_minor;
			CreateRow(farthestLeft);
		}
		if(farBorder * 1.1f > farthestRight.x){
			farthestRight += farthestRight.y < 0 ? offset_major : offset_minor;
			CreateRow(farthestRight);
		}
	}

	void BuildBoard(){
		float camSize = Camera.main.orthographicSize;
		float farBorder = camSize * Screen.width / Screen.height;
		Vector3 pos = new Vector3( -farBorder, 0);
		while(pos.x < farBorder){
			if(pos.x < farthestLeft.x){
				farthestLeft = pos;
			}
			if(pos.x > farthestRight.x){
				farthestRight = pos;
			}
			CreateRow(pos);
			if(pos.y < 0){
				pos += offset_major;
			}else{
				pos += offset_minor;
			}
		}
		/*
		for(int x = 0; x < 4; x++){
			var child = Instantiate(octave, transform.position + offset_major, transform.rotation);
			child.transform.parent = gameObject.transform;
		}
		*/
	}

	void CreateRow(Vector3 pos){
		CreateOctave(pos);
		//CreateOctave(pos + this.offset_vertical);
		//CreateOctave(pos - this.offset_vertical);

	}

	void CreateOctave(Vector3 pos){
		var child = Instantiate(octave, pos, transform.rotation);
		child.transform.parent = gameObject.transform;
	}
}
