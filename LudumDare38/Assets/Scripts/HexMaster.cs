﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HexHolder {
	public bool active;
	public HexController_ScriptableObject obj;

}
public class HexMaster : MonoBehaviour {
	[SerializeField]
	public HexHolder[] hexes;
	// Use this for initialization
	void Update () {
		foreach(HexHolder hex in hexes){
			hex.obj.IsActive = hex.active;
		}
	}

}
