﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTrigger : MonoBehaviour {
	public int pitchOffset;
	private Renderer rend;
	public HexController_ScriptableObject hexController;
	private Rigidbody2D rb;
	private PolygonCollider2D collider;
    void Start() {
        rend = GetComponent<Renderer>();
		rb = GetComponent<Rigidbody2D>();
		collider = GetComponent<PolygonCollider2D>();
		hexController.SetState(HexState.Normal);
    }

	void Update(){
		rend.material.SetColor("_EmissionColor", hexController.Current);
		if(hexController.IsActive){
			transform.localScale = new Vector3(0.9f,0.9f,0.9f);
			collider.isTrigger = true;
		}else{
			transform.localScale = new Vector3(1f, 1f, 1f);
			collider.isTrigger = false;
		}
	}
    void OnMouseEnter() {
		Debug.Log("ON MOUSE ENTER");
		hexController.SetState(HexState.Hover);
		AudioController.Instance.SetOffset(this.pitchOffset);
    }

	void OnMouseDown(){
		hexController.SetState(HexState.Click);
		AudioController.Instance.SetOffset(this.pitchOffset);
		AudioController.Instance.amplitude = 1f;
	}

	void OnMouseUp(){
		hexController.SetState(HexState.Hover);
	}

    void OnMouseExit() {
        hexController.SetState(HexState.Normal);
    }

	void OnTriggerEnter2D(Collider2D other){
		hexController.SetState(HexState.Hover);
	}

	void OnTriggerExit2D(Collider2D other){
		hexController.SetState(HexState.Normal);
	}
}
