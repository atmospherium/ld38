﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HexState {Normal, Hover, Click}
[CreateAssetMenuAttribute(fileName = "HexController", menuName = "Hex/Controller")]
[System.Serializable]
public class HexController_ScriptableObject : ScriptableObject {
	[HideInInspector]
	private bool _isActive = true;
	public bool IsActive {
		get{ return _isActive;}
		set{_isActive = value; SetState(state);}
	}
	[System.NonSerialized]
	private Color c_disabled = Color.black;
	[System.NonSerialized]
	private Color c_normal = new Color(0.3f, 0.3f, 0.3f);
	[System.NonSerialized]
	private Color c_hover = Color.green;
	[System.NonSerialized]
	private Color c_click = Color.white;
	[System.NonSerialized]
	private Color _current = new Color(0.3f, 0.3f, 0.3f);
	public Color Current {
		get{return _current;}
		private set{_current = value;}
	}

	private HexState state = HexState.Normal;
	public void SetState(HexState state){
		if(!this.IsActive){
			this.Current = c_disabled;
			return;
		}
		this.state = state;
		switch(state){
			case HexState.Hover:
				this.Current = this.c_hover;
				break;
			case HexState.Click:
				this.Current = this.c_click;
				break;
			case HexState.Normal:
			default:
				this.Current = this.c_normal;
				break;
		}
	}
}
