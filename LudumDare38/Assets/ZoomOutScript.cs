﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomOutScript : MonoBehaviour {
	Camera cam;
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		cam.orthographicSize *= ( 1 + 0.002f * Time.deltaTime);
	}
}
